import { useState, useEffect } from "react";

const SliderCircular = ({Items,Print}) => {
    const [angle, setAngle] = useState(0)
    const nItems = Items.length
    const radio = 200;
    return (
        <>
            <div className="content-slider-circular">
                <div className="slider-circular" style={{"--nItems":nItems}}> 
                    {Items.map((e,i)=>{
                        const angulo = (360 / nItems)*i + angle
                        const anguloPI = (angulo * Math.PI / 180) + (angle * Math.PI / 180)
                        const x = radio * Math.sin(anguloPI)
                        const z = radio * Math.cos(anguloPI)
                        return (
                            <div key={i} className="item-circular" style={{
                                "--n-item":i,
                                "--angulo":`${angulo}deg`,
                                "--x":`${x}px`,
                                "--z":`${z}px`,
                                }}>
                                {Print(e)}
                            </div>
                        )
                    }
                    )}
                </div>
            </div>
            <style jsx>
                {`
                    .content-slider-circular{
                        width:150px;
                        height:150px;
                        margin:150px auto;
                        position: relative;
                        perspective: 400px;
                        transform-style: preserve-3d;
                    }
                    .slider-circular{    
                        width: 100%;
                        height: 100%;
                        position: absolute;
                        transform-style: preserve-3d;
                        transition: transform 1s;
                    }
                    .item-circular{
                        width: 100%;
                        height: 100%;
                        border:1px solid #0f0f0f;
                        border-radius:10px;
                        display:flex;
                        align-items:center;
                        justify-content:center;

                        position:absolute;

                        background:#0f0f0f;
                        color:#fff;
                        transform: translateX(var(--x,0)) translateY(var(--y,0)) translateZ(var(--z,0)) rotateY(var(--angulo));
                    }
                `}
            </style>
        </>
    )
}
export default SliderCircular