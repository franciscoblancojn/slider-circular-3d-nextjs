import Head from 'next/head'

import SliderCircular from '@/components/sliderCircular'

const Items = [
  {
    id:1,
    title:"Title 1"
  },
  {
    id:2,
    title:"Title 2"
  },
  {
    id:3,
    title:"Title 3"
  },
  {
    id:4,
    title:"Title 4"
  },
  {
    id:5,
    title:"Title 5"
  },
  {
    id:6,
    title:"Title 6"
  },
]
export default function Home() {
  return (
    <div>
      <Head>
        <title>Slider Circular</title>
        <meta name="description" content="Slider Circular" />
      </Head>
      <SliderCircular 
      Items={Items}
      Print={(e)=>e.title}
      />
    </div>
  )
}
